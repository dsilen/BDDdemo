﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoCalculator
{
    public class Calculator
    {
        private Stack<int> numbers = new Stack<int>();

        public void EnterNumber(int number)
        {
            this.numbers.Push(number);
        }

        public void Add()
        {
            var num1 = numbers.Pop();
            var num2 = numbers.Pop();
            var res = num1 + num2;
            numbers.Push(res);
        }

        public int TopOfStack()
        {
            return numbers.Peek();
        }
    }
}
