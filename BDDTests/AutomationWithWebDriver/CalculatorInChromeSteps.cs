﻿using System;
using TechTalk.SpecFlow;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using FluentAssertions;

namespace BDDTests
{
    [Binding]
    public class CalculatorInChromeSteps
    {
        #region FeatureSetup
        private const string WebDriverKey = "WebDriver";

        private static ChromeDriver ChromeDriver
        {
            get { return (ChromeDriver)FeatureContext.Current[WebDriverKey]; }
        }

        [BeforeFeature()]
        public static void Setup()
        {
            System.Console.WriteLine("Setting up WebDriver");
            var webdriver = new ChromeDriver();
            FeatureContext.Current.Add(WebDriverKey, webdriver);
        }

        [AfterFeature()]
        public static void TearDown()
        {
            System.Console.WriteLine("Tearing down WebDriver");
            if(ChromeDriver != null)
            {
                ChromeDriver.Close();
                FeatureContext.Current.Clear();
            }
        }
        #endregion //FeatureSetup

        [BeforeScenario()]
        public static void ScenarioSetup()
        {
            System.Console.WriteLine("Surfing to the page");
            ChromeDriver.Navigate().GoToUrl("http://localhost:8000/calculator.elm");
        }

        [Given(@"I have entered (.*) into the calculator in web")]
        public void GivenIHaveEnteredIntoTheCalculatorInWeb(int p0)
        {
            var element = ChromeDriver.FindElementById("number-input");
            element.SendKeys(p0.ToString());
            var enter = ChromeDriver.FindElementById("enter-btn");
            enter.Click();
        }
        
        [When(@"I press add in web")]
        public void WhenIPressAddInWeb()
        {
            var element = ChromeDriver.FindElementById("add-btn");
            element.Click();        }

        [Then(@"the result should be (.*) on the screen in web")]
        public void ThenTheResultShouldBeOnTheScreenInWeb(int p0)
        {
            var element = ChromeDriver.FindElementById("result");
            var actual = element.Text;

            actual.Should().Be(p0.ToString());
        }
    }
}
