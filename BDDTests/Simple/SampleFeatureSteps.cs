﻿using TechTalk.SpecFlow;
using FluentAssertions;

namespace BDDTests
{
    using DemoCalculator;

    [Binding]
    public class SampleFeatureSteps
    {
        Calculator calc = new Calculator();

        [BeforeScenario("mytag")]
        public static void Setup()
        {
            System.Console.WriteLine("En setup som bara körs för ett scenario taggat med mytag");
        }

        [Given(@"I have entered (.*) into the calculator")]
        public void GivenIHaveEnteredIntoTheCalculator(int p0)
        {
            calc.EnterNumber(p0);
        }
        
        [Given(@"I have also entered (.*) into the calculator")]
        public void GivenIHaveAlsoEnteredIntoTheCalculator(int p0)
        {
            calc.EnterNumber(p0);
        }

        [When(@"I press add")]
        public void WhenIPressAdd()
        {
            calc.Add();
        }
        
        [Then(@"the result should be (.*) on the screen")]
        public void ThenTheResultShouldBeOnTheScreen(int p0)
        {
            calc.TopOfStack().Should().Be(p0);
        }
    }
}
