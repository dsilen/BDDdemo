import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onClick, onInput )



main =
  Html.beginnerProgram
    { model = model
    , view = view
    , update = update
    }



-- MODEL


type alias Model = 
  {
    numbers : List Int
  , inputValue : String
  , result : Int
  }


model : Model
model =
  {
    numbers = []
  , inputValue = ""
  , result = 0
  }



-- UPDATE


type Msg
  = AddNumbers
  | EnterNumber
  | InputChange String


update : Msg -> Model -> Model
update msg model =
  case msg of
    AddNumbers ->
      case model.numbers of
        [] -> model
        [_] -> model
        a::b::ns ->
          let
            added = a + b
          in
            { model |
              numbers = added :: ns
              , result = added
            }

    EnterNumber ->
      let
        inputAsInt = Result.withDefault 0 (String.toInt model.inputValue)
      in
        { model | numbers = inputAsInt :: model.numbers
          , inputValue = ""
          , result = inputAsInt
        }
    
    InputChange change ->
      { model | inputValue = change }



-- VIEW


view : Model -> Html Msg
view model =
  div []
    [
      input [ id "number-input", type_ "text", placeholder "Enter number" , onInput InputChange , value model.inputValue ] []
    , br [] []
    , button [ id "enter-btn", onClick EnterNumber ] [ text "Enter" ]
    , button [ id "add-btn", onClick AddNumbers ] [ text "Add numbers" ]
    , div [id "result"] [ text (toString model.result) ]
    ]

